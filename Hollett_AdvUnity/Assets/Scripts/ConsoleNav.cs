﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerStats;

public class ConsoleNav : MonoBehaviour
{
	// player reference
	public Player player;

	public Weapon[] weaponArray;
	public Armour[] armourArray;

	/*
	List<Weapon> weapons = new List<Weapon>();
	List<Armour> armours = new List<Armour>();
	*/

	private void Start()
	{
		/*
		// creates list for weapons
		//List<Weapon> weapons = new List<Weapon>();
		
		for (int i = 0; i < weaponArray.Length; i++)
		{
			weapons.Add(new Weapon(weaponArray[i].w_name, weaponArray[i].ItemValue, weaponArray[i].MinDamage, weaponArray[i].MaxDamage, weaponArray[i].AttackSpeed));
		}

		// creates list for armour
		//List<Armour> armours = new List<Armour>();

		for (int i = 0; i < armourArray.Length; i++)
		{
			armours.Add(new Armour(armourArray[i].a_name, armourArray[i].ItemValue, armourArray[i].DamageReduction, armourArray[i].AttackSpeedMod));
		}
		*/

		// starts choosing equipment
		//ChooseEquipmentType();
		Invoke("ChooseEquipmentType", 2f);
	}

	void ShowStats() // show current stats in console and open equipment slots
	{
		// prints all player stats to console
		print("Player Stats:");
		print("Health: " + player.Health);
		print("Attack Speed: " + player.AttackSpeed);
		print("Damage Reduction: " + player.DamageReduction);
		print("Carry Weight: " + player.CarryWeight);

		// ternary operators to check to see if any equipment is equipped
		string Weapon;
		Weapon = player.weapon == null ? "No Weapon Equipped" : player.weapon.w_name;
		print(Weapon);

		string Armour;
		Armour = player.armour == null ? "No Armour Equipped" : player.armour.a_name;
		print(Armour);
	}

	void ChooseEquipmentType() // choose what type of equipment you would like to equip
	{
		print("Press Key to Choose which equipment type to choose:");
		print("1: Weapons");
		print("2: Armour");
		print("3: Show Stats");

		
		bool keyPressed = false;
		while(keyPressed == false)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				keyPressed = true;
				ChooseWeapon();
			}
			else if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				keyPressed = true;
				ChooseArmour();
			}
			else if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				keyPressed = true;
				ShowStats();
			}
		}
		
	}

	void ChooseWeapon() // choose what weapon to equip
	{
		/*
		print("Press Key to Choose which weapon you would like to equip");
		print("1: " + weapons[0]);
		print("2: " + weapons[1]);
		print("3: " + weapons[2]);
		*/
	}

	void ChooseArmour() // choose what armour to equip
	{

	}
}
