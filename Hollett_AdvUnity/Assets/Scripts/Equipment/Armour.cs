﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerStats;

public class Armour : Equipment
{
	// count of different armours
	private static int A_Count;

	// properties of all armour
	[Range(0, 16)] public int DamageReduction;
	[Range(0, 10)] public int AttackSpeedMod;

	public string a_name;

	private void Start()
	{
		A_Count += 1;
	}

	public Armour(string newA_name, int newValue, int newDamageReduction, int newAttackSpeedMod)
	{
		a_name = newA_name;
		ItemValue = newValue;
		DamageReduction = newDamageReduction;
		AttackSpeedMod = newAttackSpeedMod;
	}
}
