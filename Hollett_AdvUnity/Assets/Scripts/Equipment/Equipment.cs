﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerStats;

public class Equipment : MonoBehaviour
{
	// properties of all equipment
	[Range(0, 200)] public float ItemWeight;
	[Range(0, 1000)] public int ItemValue;

	bool IsEquipped;

	// reference to player
	public Player player;
}
