﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerStats;

public class Weapon : Equipment
{
	// static count of weapons
	private static int W_Count;

	// properties of all weapons
	[Range(0, 30)] public int MinDamage;
	[Range(0, 30)] public int MaxDamage;
	[Range(0, 10)] public float AttackSpeed;

	public string w_name;

	private void Start()
	{
		W_Count += 1;
	}

	public Weapon(string newW_name, int newValue, int newMinDamage, int newMaxDamage, float newAttackSpeed)
	{
		w_name = newW_name;
		ItemValue = newValue;
		MinDamage = newMinDamage;
		MaxDamage = newMaxDamage;
		AttackSpeed = newAttackSpeed;
	}
}
