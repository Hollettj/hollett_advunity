﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerStats // namespace for other scripts to call and change for player stats
{
	public class PlayerStats : MonoBehaviour
	{
		public static int SetStats (int stat, int WeaponMod, int ArmourMod) // method overload for using int variable stats
		{
			stat = WeaponMod + ArmourMod;
			return stat;
		}

		public static float SetStats (float stat, float WeaponMod, float ArmourMod) // method overload for using float variable stats
		{
			stat = WeaponMod + ArmourMod;
			return stat;
		}
	}
}