﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerStats;

public class Player : MonoBehaviour
{
	// player stats
	public float AttackSpeed;
	public int Health;
	public int DamageReduction;
	public float CarryWeight;

	// equipment slots
	public Weapon weapon;
	public Armour armour;

	public void UpdateStats() // method called when equipment is equipped to update stats
	{
		CarryWeight = PlayerStats.PlayerStats.SetStats(CarryWeight, weapon.ItemWeight, armour.ItemWeight);
		AttackSpeed = PlayerStats.PlayerStats.SetStats(AttackSpeed, weapon.AttackSpeed, armour.AttackSpeedMod);
		DamageReduction = PlayerStats.PlayerStats.SetStats(DamageReduction, 0, armour.DamageReduction);
	}
}
